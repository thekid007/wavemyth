import saturn from "../images/Saturn.png";
import moon from "../images/Moon.jpg";
import jupiter from "../images/Jupiter.jpg";

/* This is the file that stores the item information. Multiple items can be added using the same pattern to extend the application. */
export default [
  {
    id: 1,
    image: saturn,
    title: "Enceladus",
    text: "is the sixth-largest moon of Saturn. It is about a tenth of Saturn's largest moon, Titan. Explore this amazing cosmic marvel in a safe and fast trip with our aerospace company.",
    price: "999.990 €",
    trip: "One way ticket",
    details: {
      title: "Enceladus",
      location: "The Saturn System",
      distance: "Distance: 9.5",
      population: "AUPopulation: 3920",
    },
  },

  {
    id: 2,
    image: moon,
    title: "Moon",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sollicitudin ligula vel tortor semper, ac laoreet lorem malesuada. Curabitur non eros accumsan, fringilla risus id, dapibus diam.",
    price: "500.000 €",
    trip: "Round trip",
    details: {
      title: "Moon",
      location: "The Solar System",
      distance: "Distance: 5",
      population: "AUPopulation: 10",
    },
  },
  {
    id: 3,
    image: jupiter,
    title: "Jupiter",
    text: " Donec pharetra vestibulum dolor, sed ultricies metus placerat sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus fermentum aliquet metus.",
    price: "750.669 €",
    trip: "One way ticket",
    details: {
      title: "Jupiter",
      location: "The Solar System",
      distance: "Distance: 11",
      population: "AUPopulation: 420",
    },
  },
];
