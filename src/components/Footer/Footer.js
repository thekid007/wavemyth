import React from "react";
import "./footer.css";

export default function Footer() {
  /* Simple footer design. Wrapped the paragraphs inside a div for easier syling. Also only one paragraph has been given
     a class name to place it slightly lower in the footer. */
  return (
    <footer className="Footer">
      <div className="TextContainer">
        <p className="Paragraph1">
          <small>Copyright COSMOS 2022. All rights reserved.</small>
        </p>
        <p>
          <small>
            All data and company references are purely fictitious and shouldn’t
            be confused with real world entities or names.
          </small>
        </p>
      </div>
    </footer>
  );
}
