import React from "react";
import "./header.css";
import home from "../../images/Home.png";
import planet from "../../images/Destinations.png";
import ship from "../../images/Spaceships.png";
import shopping from "../../images/Shopping.png";

export default function Header(props) {
  /* HTML elements for the Header. I have used a nav tag to hold all the elements.
    
     In order to have all the elements align properly I have wrapped each image + p elements inside a div that will be of display type flex.
     All the divs have the same className as they as styled simillarly.
     
     Rendered the shopping cart image and text outside the ul for easier styling using flex and space-between content justification.     
  */
  return (
    <nav className="Header">
      <ul className="ListContainer">
        <h1 className="H1">COSMOS</h1>
        <div className="MenuContainer">
          <img src={home} alt="Home" />
          <p> Home</p>
        </div>
        <div className="MenuContainer">
          <img src={planet} alt="Planet" />
          <p> Destinations</p>
        </div>
        <div className="MenuContainer">
          <img src={ship} alt="Ship" />
          <p> Spaceships</p>
        </div>
      </ul>

      <div className="MenuContainer">
        <img src={shopping} alt="shopping" />
        <p className="ShoppingCart">{props.itemsPurchased.length}</p>
      </div>
    </nav>
  );
}
