import { useState } from "react";
import "./main.css";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import Carousel from "../Carousel/CarouselItems";
import Suitcase from "../../images/Suitcase.png";
import Arrow from "../../images/Arrow.png";
import Rocket from "../../images/Rocket.png";
import Leaves from "../../images/Leaves.png";
import Checkout from "../Checkout/Checkout";

export default function Main() {
  // Create a state variable that will store and update the number of items purchased, as well as their details.
  const [purchasedItems, setPurchasedItems] = useState([]);

  /* Function that handles item purchase. It triggers upon clicking the "Purchase" button and the event is passed through props.
     This also sets the details of the items in state, which have been passed as a parameter from the Carousel Items Component. */

  function handlePurchase(item) {
    setPurchasedItems((current) => [...current, item]);
  }

  function handleCheckout() {
    setPurchasedItems([]);
  }

  /* Renders the components that make up the application: the Header, the Carousel, the Checkout and the Footer. 
     The item data is received as a parameter through the onClick event on the "Purchase" button and passed down to the Header to update the UI. 

     Filler content is rendered here (the icons, arrows below the carousel).

     Checkout section uses the same principle as the carousel, the checkout button using an onClick event to trigger state reset here.    

     */

  return (
    <div className="Main">
      <Header itemsPurchased={purchasedItems} />
      <Carousel onItemPurchase={handlePurchase} />
      <div className="ImagesContainer">
        <div className="Pair">
          <img src={Suitcase} alt="suitcase" />
          <h3>pack</h3>
        </div>
        <img src={Arrow} alt="arrow" />
        <div className="Pair">
          <img src={Rocket} alt="rocket" />
          <h3>fly</h3>
        </div>
        <img src={Arrow} alt="arrow" />
        <div className="Pair">
          <img src={Leaves} alt="leaves" />
          <h3>live</h3>
        </div>
        <img src={Arrow} alt="arrow" />
        <div className="Enjoy">
          <h1>ENJOY</h1>
          <h3>A NEW WORLD!</h3>
        </div>
      </div>
      <Checkout onCheckout={handleCheckout} />
      <Footer />
    </div>
  );
}
