import { Carousel } from "react-responsive-carousel";
import data from "../../utils/data.js";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./carousel.css";

/* Carousel component. This uses the data of every destination from the data.js file and maps through each item in the array.
   Each item contains specific information and the onClick event on the "Purchase" button will send the information up to the Main component 
   as a parameter to be used to update the UI. */

export default function CarouselItems(props) {
  const carouselData = data.map((item) => {
    return (
      <div className="CarouselContent">
        <div className="DetailsContainer">
          <img src={item.image} className="CarouselImage" />
          <div className="DetailsTextContainer">
            <h3>{item.details.title}</h3>
            <p>{item.details.location}</p>
            <p>{item.details.distance}</p>
            <p>{item.details.population}</p>
          </div>
        </div>

        <div className="TextContainer">
          <h1 className="CarouselTitle">{item.title}</h1>
          <p className="CarouselText">{item.text}</p>
          <div className="PurchaseContainer">
            <div className="Purchase">
              <h1>{item.price}</h1>
              <p>{item.trip}</p>
            </div>
            <button
              type="button"
              className="PurchaseButton"
              onClick={() => props.onItemPurchase(item)}
            >
              Purchase
            </button>
          </div>
        </div>
      </div>
    );
  });
  return (
    <div className="CarouselContainer">
      <Carousel showStatus={false}>{carouselData}</Carousel>
    </div>
  );
}
